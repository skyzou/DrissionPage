目前设置功能只支持 Chrome 浏览器，如要使用其它浏览器，可使用 selenium 原生方法创建 driver，然后用 Drission 对象接收即可。

!> **注意：** <br>本库所有功能暂时只对 Chrome 做了完整测试。

```python
from selenium import webdriver
from DrissionPage import Drission, MixPage

# 用 selenium 原生代码创建 WebDriver 对象
driver = webdriver.Firefox()
# 把 WebDriver 对象传入 Drission 对象
dr = Drission(driver_or_options=driver)

page = MixPage(drission=dr)
page.get('https://www.baidu.com')
```